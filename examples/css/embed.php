<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<link rel="stylesheet" href="/css/embed.css" name="www-player">
<link rel="stylesheet" href="/vjds/v5/skins/shaka/videojs.min.css" name="www-player">
<script src="/vdjs/core/7.6/video.min.js" type="text/javascript"></script>
<script src="/vdjs/v5/nuevo.min" type="text/javascript"></script>
<title>Nuevo plugin version5 and Shaka skin for Video.js player</title>
<link rel="canonical" href="https://www.nuevodevel.com/nuevo/">
</head>
<body>
<div class="player">
<video id="veoplayer" class="video-js vjs-fluid vjs-default-skin"  poster="//devnuevo.com/media/video/people.jpg" controls preload="auto" playsinline webkit-playsinline></video>
</div>
<script type="text/javascript" src="/vdjs/v5/js/embed.js"></script>
</body>
</html>
