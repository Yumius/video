
/**
 * Nuevo plugin
 * @version 5.4.0
 * @copyright Nuevodevel, Inc.
 */

Nuevo plugin for Videojs framework version 6.x and 7.x

Video.js player filename used in examples  is "video.min.js", located in "videojs" directory.
This is minimized video.js script, version 7.6
Full version ov videojs v.7.6 is located in the same directory.
You may use any CDN hosted video.js version above 6.4 version.

==========================================================

Upload "videojs" and "examples" directories to your server.
Navigate to "/examples/index.html" for multiple HTML examples covering most popular videojs and nuevo plugin usage.
